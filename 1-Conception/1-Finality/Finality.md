# Finality

## Objectif

Document(s) permettant de définir la finalité de l’ontologie

## Description

Quel son but, son usage clef ?
A qui et à quoi va-t-elle servir ?

## Comment

Il est pertinent d'identifier les 3 choses suivantes :
* Les objectifs à atteindre
* Les facteurs influençants (éléments perturbants le fonctionnement actuel)
* Les initiatives imaginées pour répondre aux objectifs tout en contrant les influences