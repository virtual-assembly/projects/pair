# PAIR ontology process

## Overview

This document describes the workflow being followed to update the [PAIR ontology](https://www.virtual-assembly.org/ontologies/pair).

## WHO : The Steering Group and you

## Rationale

The Virtual Assembly seeks to find a balance between the openness of PAIR as a common good to which anyone is invited to contribute, and the necessity to keep PAIR consistent and useful for its users.

## The community : everyone

Anyone with a use-case is welcome to describe it, and is welcome to propose evolutions in the PAIR ontology if its use-case is not adressed.

The proposed evolutions in PAIR are managed and publicly visible at http://wekan.virtual-assembly.org/b/TjHLBmxD2T6H3Yq8v/ontologies. Anyone with an account can enter a new item in the backlog; registration is opened to anyone.

## The PAIR steering group

The PAIR steering group is responsible for high level oversight of the project, discuss the proposed evolutions, and prepares the releases (see below).

The PAIR steering groups decides on what will be included in future releases of PAIR, based on its understanding of the ecosystem and users.

The PAIR steering group is composed of:
  - *Business experts*, representatives from the needs and the user community of PAIR;
  - *Knowledge architects*, specialized in the identification, description and linking of the core business concepts;
  - *Ontologists*, capable of implementing the semantic modeling of business concepts into an operational ontology;

For specific topics that have been raised by a specific user or user community, this user or a representative of the user community will be invited to participate to the steering group discussions on that topic, and will be part of the decision for that item.

Usually the process will look like this one:
  1. A need is identified from the user community or directly from a business expert;
  2. A knowledge architect formalize the need into a semantic map of business concepts,
  3. An ontologist proposes a formal implementation of the evolution in PAIR;
  4. The steering group validates the evolutions;

The PAIR steering group takes its decision based on consensus : every member must explicitely agree to all decisions. If consensus cannot be reached, the Business experts have the final decision.

The steering group members are currently:
  - Guillaume Rouyer
  - Bernard Chabot
  - Thomas Francart 

New members are invited upon a consensus decision of the steering group members.


## WHEN : Release periodicity

### Periodicity

The PAIR ontology will be reviewed _twice a year_, and published at solstice of summer and solstice of winter (06-21 and 12-21) each year.

### Public review

1 month between each release date of PAIR, the steering group will publish a release candidate for that version (on 05-21 and 11-21). If no concerns are expressed within a month, the release is officially published.

## Versioning and change control

The versions of PAIR will use [semantic versioning](https://semver.org/), with 3-numbers version numbers MAJOR.MINOR.PATCH :

- An increment in MAJOR means something is _not backward compatible_ with previous releases of the ontology. Typically a URI was deleted, domain or ranges of some properties were modified in an incompatible way;
- An increment in MINOR means new functionnalities were added that are backward-compatible with previous versions. Typically new classes or properties are introduced without modifying th rest;
- An increment in PATCH means bug fixes were done but no functionalities are added;

Additionnally, each release of PAIR will be referred to with a code name corresponding to the season at which it was published : Summer20XX or Winter 20XX.

## HOW

### Meetings

The PAIR steering group meets in phone/video meetings every 2 weeks.
The conclusions of each meeting are released publicly at ...

### Discussion channel

The PAIR steering group has a discussion channel [Matrix / Riot](https://riot.im/app/#/room/#ontologies:matrix.virtual-assembly.org) ...